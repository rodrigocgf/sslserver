// SSLFrame.cpp : implementation file
//

#include "stdafx.h"
#include "CHBSSLServer.h"
#include "SSLFrame.h"
#include "ThrTransacao.h"

#include "LogApiFunc.h"
#include <time.h>
#include <string.h>
#include <stdio.h>


extern CLogApiFunc logapi;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSSLFrame

IMPLEMENT_DYNCREATE(CSSLFrame, CFrameWnd)

CSSLFrame::CSSLFrame()
{
	h_RespOK = CreateEvent(NULL,0,0,NULL);

	m_pMySocket = new CMySocket( &m_SslTrustedCertHashList);
	m_pMySocket->p_Parent = this;	
	m_pMySocket->AddLayer(m_pMySocket->m_pSslLayer);	
	m_pMySocket->Create();
	m_pMySocket->m_pSslLayer->InitClientSSL();

	InitializeCriticalSection(&m_CriticalSection);
}

CSSLFrame::~CSSLFrame()
{
	if ( m_pMySocket ) {
		m_pMySocket->m_pSslLayer->UnloadSSL();
		m_pMySocket->Close();		

		delete m_pMySocket;
	}

	DeleteCriticalSection(&m_CriticalSection);
}


BEGIN_MESSAGE_MAP(CSSLFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CSSLFrame)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_USER + 2, OnVerifyCert)
	ON_MESSAGE(WM_USER + 3, OnSSLConnect)
	ON_MESSAGE(WM_USER + 4, OnLojaLogada)
	ON_MESSAGE(WM_USER + 5, OnDados)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSSLFrame message handlers
int CSSLFrame::ConsultaCheque( char * buffer_in, 
							   char * buffer_out, 
							   char * ip_endereco, 
							   char * ip_porta )
{

	char	buffer_rec[TAM_BUF_RECEB_NEW];
	int		resp = -1;
	
	m_buffer_in = buffer_in;
	m_buffer_out = buffer_out;
	m_ip_endereco = ip_endereco;
	m_ip_porta = ip_porta;

	memset (buffer_rec,0, sizeof(buffer_rec));	
	if ( (resp = IniciaComunicacao(ip_endereco, ip_porta)) < 0 ) {
		logapi.logtrc ( "ConsultaCheque >> Falha no inicia comunicacao");				
	}
	return resp;
}

int CSSLFrame::IniciaComunicacao(char * ip_endereco, char * ip_porta)
{	
	int i_porta;	

	i_porta = atoi(ip_porta);	
	BOOL res=m_pMySocket->Connect(ip_endereco,i_porta);		
	logapi.logtrc("m_pMySocket->Connect(%s,%d)",ip_endereco,i_porta);		
	AddStringToLog("Connecting, please wait...");	
	
	return 1;
	
}

void CSSLFrame::AddStringToLog(LPCTSTR pszString)
{
	logapi.logtrc((char *)pszString);
	return;
}

LRESULT CSSLFrame::OnVerifyCert(WPARAM wParam, LPARAM lParam)
{
	m_pMySocket->m_pSslLayer->SetNotifyReply(m_pMySocket->pData->priv_data, SSL_VERIFY_CERT, 1);

	return 1;
}

BOOL CSSLFrame::PreCreateWindow(CREATESTRUCT& cs) 
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.lpszClass = AfxRegisterWndClass(0);
	return TRUE;
}

int CSSLFrame::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	// create a view to occupy the client area of the frame
	if (!m_wndView.Create(NULL, NULL, AFX_WS_DEFAULT_VIEW,
		CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST, NULL))
	{
		TRACE0("Failed to create view window\n");
		return -1;
	}

	return 0;
}

LRESULT CSSLFrame::OnSSLConnect(WPARAM wParam, LPARAM lParam)
{
	// Envia o Pacote de Login

	int resp;
	char sz_SenhaLoja[9];
	
	logapi.logtrc("EnviaPacoteLogin");
	// preencha a estrutura de login
	memcpy(&m_stLogin.sz_NomeTrans[0],"ACS2",4); // nome da transa��o
	memcpy(&m_stLogin.sz_TamString,"0059",4); // tamanho total da estrutura ( compatibilidade )
	strcpy(&m_stLogin.sz_IDMsg[0],"L00");
	memcpy(&m_stLogin.sz_IdTerm[0],&m_buffer_in[4],8); // id do terminal
	
	memcpy(sz_SenhaLoja,"000",3);
	memcpy(&sz_SenhaLoja[3],&m_buffer_in[32],5);
	memcpy(&m_stLogin.sz_SenhaLoja[0],&sz_SenhaLoja[0],8); // senha da loja
	
	memcpy(m_stLogin.sz_Delay,"0000",4);
	memcpy(m_stLogin.sz_TamBlocosResp,"0000",4);
	m_stLogin.ch_flagBanner = ' ';
	memcpy(m_stLogin.sz_filler,"                      ",22);
	m_stLogin.ch_StatusMem = 'N';
	m_stLogin.sz_Terminador[0] = '^';
	m_stLogin.sz_Terminador[1] = 'C';
	m_stLogin.sz_Enter [0] = '\n';
	
	//if ( socketSessao.WriteT( (char *)&m_stLogin, sizeof(st_LOGIN), 20*1000 ) >= 0 ) {
	if ( m_pMySocket->m_pSslLayer->Send((char *)&m_stLogin, sizeof(st_LOGIN), 0 ) >= 0 ) {
		logapi.logdump("PACOTE DE LOGIN : ",(char *)&m_stLogin,sizeof(st_LOGIN));
		resp = 0;
	} else {
		logapi.logerr ( "Falha no envio do Login : {", &m_stLogin, "} tamanho: ", sizeof(st_LOGIN) );
		resp = -1;
	}



	return 1;
}

LRESULT CSSLFrame::OnLojaLogada(WPARAM wParam, LPARAM lParam)
{
	// envia os dados
	char* psz_send;
	char* psz_send_final;
	int i_sendindex;
	char sz_checksum[8+1];
	
	logapi.logtrc("CSSLFrame::OnLojaLogada");

	EnterCriticalSection(&m_CriticalSection);

	// LIMPA A ESTRUTURA DE SOLICITA��O DE CONSULTA
	memset((char *)&m_stSolicitacaoConsulta,0x00,sizeof(st_SOLICITACAO_CONSULTA));
	
	// PREENCHIMENTO DA ESTRUTURA
	// DE SOLICITA��O DE CONSULTA
	memcpy(&m_stSolicitacaoConsulta.sz_NomeTrans[0],"ACS2",4);
	m_stSolicitacaoConsulta.ch_VersaoLayout = '2';
	strcpy(m_stSolicitacaoConsulta.sz_IdTipoMsg,"010");
	m_stSolicitacaoConsulta.ch_flag_Continua = ' ';
	memcpy(m_stSolicitacaoConsulta.sz_CNPJ_LOJA,&m_buffer_in[12],15);
	//m_stSolicitacaoConsulta.sz_CNPJ_LOJA = ' ';
	m_stSolicitacaoConsulta.ch_TipoTerminal = '1';
	m_stSolicitacaoConsulta.ch_TipoConexao = '3';
	memcpy(&m_stSolicitacaoConsulta.sz_NumeroContrato[0],&m_buffer_in[27],5);
	memcpy(&m_stSolicitacaoConsulta.sz_SenhaContrato[0],&m_buffer_in[32],15);
	m_stSolicitacaoConsulta.ch_MotivoTransacao = '1';
	m_stSolicitacaoConsulta.ch_TipoDocumento = m_buffer_in[37];
	memcpy((char *)&m_stSolicitacaoConsulta.sz_CNPJ_CPF[0],&m_buffer_in[38],15);
	memcpy((char *)&m_stSolicitacaoConsulta.sz_DDD[0],&m_buffer_in[53],3);		
	memcpy((char *)&m_stSolicitacaoConsulta.sz_Telefone[0],&m_buffer_in[56],8);

	//memset((char *)&m_stSolicitacaoConsulta.sz_OperacoesAdicionais[0],' ',5);
	memcpy((char *)&m_stSolicitacaoConsulta.sz_OperacoesAdicionais[0],"FORTE",5);
	memcpy((char *)&m_stSolicitacaoConsulta.sz_QtdeCheques[0],&m_buffer_in[65],2);
	m_stSolicitacaoConsulta.ch_FormaEntrada = m_buffer_in[64];
	memcpy((char *)&m_stSolicitacaoConsulta.sz_CMC7[0],&m_buffer_in[67],30);
	memcpy((char *)&m_stSolicitacaoConsulta.sz_DataCheque[0],&m_buffer_in[97],8);
	memcpy((char *)&m_stSolicitacaoConsulta.sz_ValorChequeCentavos[0],&m_buffer_in[105],15);
	
	// MONTA O PACOTE DE ENVIO COM CAMPOS SEPARADOS POR '|'
	psz_send_final = new char[( sizeof(st_SOLICITACAO_CONSULTA) + 19 + 18 + 1) ];
	memset(psz_send_final,0x00,( sizeof(st_SOLICITACAO_CONSULTA) + 19 + 18 + 1) );
	
	psz_send = new char[( sizeof(st_SOLICITACAO_CONSULTA) + 19 )];
	memset(psz_send,0x00,( sizeof(st_SOLICITACAO_CONSULTA) + 19 ) );
	
	
	i_sendindex = 0;
	memcpy(&psz_send[i_sendindex],(char *)&m_stSolicitacaoConsulta.sz_NomeTrans[0],4);
	i_sendindex += 4;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	psz_send[i_sendindex] = m_stSolicitacaoConsulta.ch_VersaoLayout;
	i_sendindex += 1;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	memcpy(&psz_send[i_sendindex],&m_stSolicitacaoConsulta.sz_IdTipoMsg[0],3);
	i_sendindex += 3;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	//	psz_send[i_sendindex] = m_stSolicitacaoConsulta.ch_flag_Continua;
	//	memcpy(&psz_send[i_sendindex],(char *)&m_stSolicitacaoConsulta.ch_flag_Continua[0],1);
	//	i_sendindex += 1;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	//psz_send[i_sendindex] = m_stSolicitacaoConsulta.sz_CNPJ_LOJA;
	memcpy(&psz_send[i_sendindex],&m_stSolicitacaoConsulta.sz_CNPJ_LOJA[0],15);
	i_sendindex += 15;
	//i_sendindex += 1;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	psz_send[i_sendindex] = m_stSolicitacaoConsulta.ch_TipoTerminal;
	i_sendindex += 1;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	psz_send[i_sendindex] = m_stSolicitacaoConsulta.ch_TipoConexao;
	i_sendindex += 1;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	memcpy(&psz_send[i_sendindex],&m_stSolicitacaoConsulta.sz_NumeroContrato[0],5);
	i_sendindex += 5;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	memcpy(&psz_send[i_sendindex],&m_stSolicitacaoConsulta.sz_SenhaContrato[0],5);
	i_sendindex += 5;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	psz_send[i_sendindex] = m_stSolicitacaoConsulta.ch_MotivoTransacao;
	i_sendindex += 1;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	psz_send[i_sendindex] = m_stSolicitacaoConsulta.ch_TipoDocumento;
	i_sendindex += 1;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	memcpy(&psz_send[i_sendindex],&m_stSolicitacaoConsulta.sz_CNPJ_CPF[0],15);
	i_sendindex += 15;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	memcpy(&psz_send[i_sendindex],&m_stSolicitacaoConsulta.sz_DDD[0],3);
	i_sendindex += 3;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	memcpy(&psz_send[i_sendindex],&m_stSolicitacaoConsulta.sz_Telefone[0],8);
	i_sendindex += 8;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	memcpy(&psz_send[i_sendindex],&m_stSolicitacaoConsulta.sz_OperacoesAdicionais[0],5);
	i_sendindex += 5;
	
	//memcpy(&psz_send[i_sendindex],&m_stSolicitacaoConsulta.sz_OperacoesAdicionais[0],1);
	//i_sendindex += 1;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	memcpy(&psz_send[i_sendindex],&m_stSolicitacaoConsulta.sz_QtdeCheques[0],2);
	i_sendindex += 2;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	psz_send[i_sendindex] = m_stSolicitacaoConsulta.ch_FormaEntrada;
	i_sendindex += 1;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	memcpy(&psz_send[i_sendindex],&m_stSolicitacaoConsulta.sz_CMC7[0],30);
	i_sendindex += 30;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	memcpy(&psz_send[i_sendindex],&m_stSolicitacaoConsulta.sz_DataCheque[0],8);
	i_sendindex += 8;
	
	psz_send[i_sendindex] = '|';
	i_sendindex += 1;
	
	memcpy(&psz_send[i_sendindex],&m_stSolicitacaoConsulta.sz_ValorChequeCentavos[0],15);
	i_sendindex += 15;
	
	// preende psz_send_final
	psz_send_final[0] = '@';
	char ch_tam[4+1];
	memset(ch_tam,0x00,5);
	sprintf(ch_tam,"%04d",i_sendindex);
	memcpy(&psz_send_final[1],ch_tam,4);
	
	int i_checksum = 0;
	for ( int i_1 = 0 ; i_1 < i_sendindex ; i_1++ ) {
		i_checksum += psz_send[i_1];
	}
	
	memset(sz_checksum,0x00,9);
	sprintf(sz_checksum,"%08d",i_checksum);	
	memcpy(&psz_send_final[5],sz_checksum,8);	
	memcpy(&psz_send_final[13],"00",2);
	
	psz_send_final[15] = '@';
	
	memcpy(&psz_send_final[16],&psz_send[0],i_sendindex);
	
	psz_send_final[16 + i_sendindex] = '^';
	psz_send_final[17 + i_sendindex] = 'C';
	psz_send_final[18 + i_sendindex] = '\n';
	
		
	//if ( socketSessao.WriteT( psz_send_final, (i_sendindex + 18 + 1), 20*1000 ) < 0 ) {
	if ( m_pMySocket->m_pSslLayer->Send( psz_send_final, (i_sendindex + 18 + 1), 0 ) < 0 ) {
		logapi.logerr("EnviaDadosCheque >> Erro no envio da Solicita��o de Consulta");
		return -1;
	} else {
		logapi.logdump("DADOS ENVIADOS : ",(char *)psz_send_final,(i_sendindex + 19));
	}
	
	
	if (psz_send)
		delete [] psz_send;
	
	if (psz_send_final)
		delete [] psz_send_final;

	LeaveCriticalSection(&m_CriticalSection);

	return 1;
}

LRESULT CSSLFrame::OnDados(WPARAM wParam, LPARAM lParam)
{

	char ch_aux;
	int i_1 ,i_2 , i_mapcount;
	int i_tamTrans;
	char s_tam_inicio[4];
	char s_descricao [32];	
	int i_QtdeInfo;
	char* psz_ToComp;
	int trans;
	char transacao[11];
	int flg_dados = 0;
	int i_tc= 38;
	char s_data[8];	
	char* psz_recv;

	memcpy(s_tam_inicio, &m_pMySocket->s_recbuffer[1], 4);
	//logapi.logdump ("s_tam_inicio:", (char *) s_tam_inicio, 4);
	i_tamTrans = atoi(s_tam_inicio);

	if ( i_tamTrans < 0 || i_tamTrans > 512 ) {
		logapi.logtrc("RecebeDadosCheque >> Tamanho do pacote de resposta recebido inv�lido");		
		return -1;
	}

	EnterCriticalSection(&m_CriticalSection);

	psz_recv = new char[i_tamTrans];

	memset(psz_recv,0,sizeof(psz_recv));
	memcpy(psz_recv,&m_pMySocket->s_recbuffer[19], i_tamTrans);
	
	// PEGA UM CAMPO A CADA '|'
	// Mapeia < N�MERO DA '|' > COM < POSI��O DA '|' NA STRING>
	m_MapBarras.erase(m_MapBarras.begin(),m_MapBarras.end());
	i_mapcount = 1;
	for ( i_1 = 0 ; i_1 < i_tamTrans ; i_1++ ) {
		ch_aux = psz_recv[i_1];
		if ( ch_aux == '|' ) {
			m_MapBarras[i_mapcount] = i_1;
			i_mapcount++;
		}
	}	
	
	i_1 = 1;
	
	memcpy(&m_stRespostaConsulta.sz_NomeTrans[0],(char *)&psz_recv[0],m_MapBarras[i_1]);
	
	i_2 = m_MapBarras[i_1];
	i_1++;
	
	
	if ( (m_MapBarras[i_1] - i_2) > 1 ) {
		m_stRespostaConsulta.ch_VersaoLayout = psz_recv[(i_2 + 1)];	
	} else {
		m_stRespostaConsulta.ch_VersaoLayout = ' ';
	}
	
	i_2 = m_MapBarras[i_1];
	i_1++;	
	
	
	if ( (m_MapBarras[i_1] - i_2) > 1 ) {
		memcpy(&m_stRespostaConsulta.sz_IdTipoMsg[0],&psz_recv[(i_2+1)],(m_MapBarras[i_1] -  i_2 - 1));
	} else {
		memcpy(&m_stRespostaConsulta.sz_IdTipoMsg[0],"   ",3);
	}
	
	i_2 = m_MapBarras[i_1];
	i_1++;		
	

	// ERRO CONSULTA / STATUS CONSULTA
	if (!memcmp (m_stRespostaConsulta.sz_IdTipoMsg, "060",3) ||
		!memcmp (m_stRespostaConsulta.sz_IdTipoMsg, "030",3))
	{
	
		if ( (m_MapBarras[i_1] - i_2) > 1 ) {
			memcpy(sz_Qtd_Campos,&psz_recv[(i_2 + 1)],(m_MapBarras[i_1] - i_2 - 1));
		} else {
			memcpy(sz_Qtd_Campos,"      ",6);
		}
	
		i_2 = m_MapBarras[i_1];
		i_1++;


		if ( (m_MapBarras[i_1] - i_2) > 1 ) {
			memcpy(cod_status,&psz_recv[(i_2 + 1)],(m_MapBarras[i_1] - i_2 - 1));
		} else {
			memcpy(cod_status," ",1);
		}
	
		i_2 = m_MapBarras[i_1];
		i_1++;

		
		if (m_MapBarras[i_1] < 1 )
		{
			m_MapBarras[i_1] = i_tamTrans;
		
		}

		if ( (m_MapBarras[i_1] - i_2) > 1 ) {
			memcpy(s_descricao,&psz_recv[(i_2 + 1)],(m_MapBarras[i_1] - i_2 - 1));
		} else {
			memcpy(s_descricao,"                                ",1);
		}
	
		i_2 = m_MapBarras[i_1];
		i_1++;

		memset(s_buffer_dll, '\0', 300);
		memcpy(s_buffer_dll,"RX22",4);
	
	
		//Pega data e hora da resposta
		GetDataHoraSolicitacao();
		memcpy(&s_buffer_dll[4],datahora_solicitacao,14);
	
		// ID DO TERMINAL
		memcpy(&s_buffer_dll[18],m_pMySocket->m_stRespLogin.sz_IdTerm,8);
	
		// STATUS DA TRANSA��O	
		memcpy(&s_buffer_dll[26],cod_status,1);
		memcpy(&s_buffer_dll[27], s_descricao, strlen(s_descricao));
	
		i_len_bufdll = 60;

		((CThrTransacao *)p_ThrTrans)->PostThreadMessage(WM_USER + 5,0,0); // avisa que chegou uma resposta

		
		if ( psz_recv )
			delete [] psz_recv;

		LeaveCriticalSection(&m_CriticalSection);

		return 1;
	}

	
	if ( (m_MapBarras[i_1] - i_2) > 1 ) {
		memcpy(&m_stRespostaConsulta.sz_QtdeCampos[0],&psz_recv[(i_2 + 1)],(m_MapBarras[i_1] - i_2 - 1));
	} else {
		memcpy(&m_stRespostaConsulta.sz_QtdeCampos[0],"   ",3);
	}
	
	i_2 = m_MapBarras[i_1];
	i_1++;	
	
	
	if ( (m_MapBarras[i_1] - i_2) > 1 ) {
		m_stRespostaConsulta.ch_FlagOperAdic = psz_recv[(i_2 + 1)];	
	} else {
		m_stRespostaConsulta.ch_FlagOperAdic = ' ';
	}
	
	i_2 = m_MapBarras[i_1];
	i_1++;	
	
	
	if ( (m_MapBarras[i_1] - i_2) > 1 ) {
		m_stRespostaConsulta.ch_FlagHaMaisCampos = psz_recv[(i_2 + 1)];	
	} else {
		m_stRespostaConsulta.ch_FlagHaMaisCampos = ' ';
	}
	
	i_2 = m_MapBarras[i_1];
	i_1++;	
		
	if ( (m_MapBarras[i_1] - i_2) > 1 ) {
		memcpy(&m_stRespostaConsulta.sz_CodResposta[0],&psz_recv[(i_2 + 1)],(m_MapBarras[i_1] - i_2 - 1));		
	} else {
		memset(&m_stRespostaConsulta.sz_CodResposta[0],' ',2);
	}
	

	i_2 = m_MapBarras[i_1];
	i_1++;	
	
	
	if ( (m_MapBarras[i_1] - i_2) > 1 ) {
		memset(m_stRespostaConsulta.sz_PrimeiroTexto,' ',16);
		memcpy(&m_stRespostaConsulta.sz_PrimeiroTexto[0],&psz_recv[(i_2 + 1)],(m_MapBarras[i_1] - i_2 - 1));
	} else {
		memcpy(&m_stRespostaConsulta.sz_PrimeiroTexto[0],"                ",16);
	}
	
	i_2 = m_MapBarras[i_1];
	i_1++;	
	
	
	
	
	if ( (m_MapBarras[i_1] - i_2) > 1 ) {
		memset(m_stRespostaConsulta.sz_SegundoTexto,' ',16);
		memcpy(&m_stRespostaConsulta.sz_SegundoTexto[0],&psz_recv[(i_2 + 1)],(m_MapBarras[i_1] - i_2 - 1));
	} else {
		memcpy(&m_stRespostaConsulta.sz_SegundoTexto[0],"                ",16);
	}
	
	i_2 = m_MapBarras[i_1];
	i_1++;	
	
	
	if ( (m_MapBarras[i_1] - i_2) > 1 ) {
		m_stRespostaConsulta.ch_FlagChequesConsultados = psz_recv[(i_2 + 1)];	
	} else {
		m_stRespostaConsulta.ch_FlagChequesConsultados = ' ';
	}
	
	i_2 = m_MapBarras[i_1];
	i_1++;	
	
	
	if ( (m_MapBarras[i_1] - i_2) > 1 ) {
		memcpy(&m_stRespostaConsulta.sz_NumConsulta[0],&psz_recv[(i_2 + 1)],(m_MapBarras[i_1] - i_2 - 1));
	} else {
		memcpy(&m_stRespostaConsulta.sz_NumConsulta[0],"                    ",20);
	}
	
	i_2 = m_MapBarras[i_1];
	i_1++;	
	
	
	
	if ( (m_MapBarras[i_1] - i_2) > 1 ) {
		memcpy(&m_stRespostaConsulta.sz_DataConsulta[0],&psz_recv[(i_2 + 1)],(m_MapBarras[i_1] - i_2 - 1));
	} else {
		memcpy(&m_stRespostaConsulta.sz_DataConsulta[0],"          ",10);
	}
	
	i_2 = m_MapBarras[i_1];
	i_1++;	
	
	
	
	if ( i_1 < i_mapcount ) { // h� mais barras
		if ( (m_MapBarras[i_1] - i_2) > 1 ) {
			memcpy(&m_stRespostaConsulta.sz_HoraConsulta[0],&psz_recv[(i_2 + 1)],(m_MapBarras[i_1] - i_2 - 1));
		} else {
			memcpy(&m_stRespostaConsulta.sz_HoraConsulta[0],"        ",8);
		}
		
		i_2 = m_MapBarras[i_1];
		i_1++;	
		
	} else {
		// COPIA OS PR�XIMOS 8 CARACTERES
		memcpy(&m_stRespostaConsulta.sz_HoraConsulta[0],&psz_recv[(i_2 + 1)],8);
	}
	
	
	if (m_stRespostaConsulta.ch_FlagOperAdic == 'S') {
		
		if ( (m_MapBarras[i_1] - i_2) > 1 ) {
			memcpy(&m_stRespostaConsulta.sz_OperAdicional[0],&psz_recv[(i_2 + 1)],(m_MapBarras[i_1] - i_2 - 1));
		} else {
		
			memset(&m_stRespostaConsulta.sz_OperAdicional[0],' ',10);
		}
		
		i_2 = m_MapBarras[i_1];
		i_1++;	
		
		
		if ( (m_MapBarras[i_1] - i_2) > 1 ) {
			m_stRespostaConsulta.ch_FlagTemMaisOperAdic = psz_recv[(i_2 + 1)];
		} else {
			m_stRespostaConsulta.ch_FlagTemMaisOperAdic = ' ';
		}
		
		i_2 = m_MapBarras[i_1];
		i_1++;	
		
		memset(&m_stRespostaConsulta.sz_QtdeInformacoes[0],'\0',5);
		
		
		if ( (m_MapBarras[i_1] - i_2) > 1 ) {
			memcpy(&m_stRespostaConsulta.sz_QtdeInformacoes[0],&psz_recv[(i_2 + 1)],(m_MapBarras[i_1] - i_2 - 1));
		} else {
		
			memset(&m_stRespostaConsulta.sz_QtdeInformacoes[0],' ',5);
		}
		
		i_2 = m_MapBarras[i_1];
		i_1++;
		
		
		i_QtdeInfo = atoi(&m_stRespostaConsulta.sz_QtdeInformacoes[0]);		
		
		if ( i_QtdeInfo < 0 || i_QtdeInfo > 20 )  {
			logapi.logtrc("RecebeDadosCheque >> Erro na quantidade de informa��es ");

			if ( psz_recv )
				delete [] psz_recv;

			LeaveCriticalSection(&m_CriticalSection);

			return -1;
		}
		
		// esvazia a fila
		m_queue_DETALHE.empty();
		
		for ( int ii_2 = 0 ; ii_2 < i_QtdeInfo ; ii_2++ ) {
				
			m_DETALHE_CONSULTA.ch_FlagInfDestino = ' ';
		
			if ( (m_MapBarras[i_1] - i_2) > 1 ) {
				m_DETALHE_CONSULTA.ch_FlagInfDestino = psz_recv[(i_2 + 1)];
				
			} else {
				m_DETALHE_CONSULTA.ch_FlagInfDestino = ' ';
				
			}
			
			i_2 = m_MapBarras[i_1];
			i_1++;
			
			
			
			memset(m_DETALHE_CONSULTA.sz_Titulo,0x00,30);
			if ( (m_MapBarras[i_1] - i_2) > 1 ) {
				memcpy(&m_DETALHE_CONSULTA.sz_Titulo[0],&psz_recv[(i_2 + 1)],(m_MapBarras[i_1] - i_2 - 1));
				
			} else {
				memset(&m_DETALHE_CONSULTA.sz_Titulo[0],' ',30);
				
			}
			
			i_2 = m_MapBarras[i_1];
			i_1++;
			
			
			
			memset(m_DETALHE_CONSULTA.sz_Conteudo,0x00,100);
		

			if (m_MapBarras[i_1] < 1 )
			{
				m_MapBarras[i_1] = i_tamTrans;
			
			}
							
			if ( (m_MapBarras[i_1] - i_2) > 1 ) {
				memcpy(&m_DETALHE_CONSULTA.sz_Conteudo[0],&psz_recv[(i_2 + 1)],(m_MapBarras[i_1] - i_2 - 1));
	
			} else {
				memset(&m_DETALHE_CONSULTA.sz_Conteudo[0],' ',30);
	
			}
			
			i_2 = m_MapBarras[i_1];
			i_1++;
				
			m_queue_DETALHE.push(m_DETALHE_CONSULTA);
			
		}
	} else {
		// PREENCHE COM BRANCOS 
		memset(&m_stRespostaConsulta.sz_OperAdicional[0],' ',10);
		m_stRespostaConsulta.ch_FlagTemMaisOperAdic = ' ';
		memset(&m_stRespostaConsulta.sz_QtdeInformacoes[0],' ',5);

		i_QtdeInfo = 0;
		// esvazia a fila
		m_queue_DETALHE.empty();

		for ( int ii_2 = 0 ; ii_2 < i_QtdeInfo ; ii_2++ ) {
			m_DETALHE_CONSULTA.ch_FlagInfDestino = ' ';
			memset(&m_DETALHE_CONSULTA.sz_Titulo[0],' ',30);
			memset(&m_DETALHE_CONSULTA.sz_Conteudo[0],' ',30);

			m_queue_DETALHE.push(m_DETALHE_CONSULTA);
		}

	}
	

	if ( psz_recv )
		delete [] psz_recv;

	
	/*
		
		  LAYOUT DO BUFFER PARA O COMPONENTE
		  
			RX21					-	4 CARACTERES	-	4	
			NSU						-	10 CARACTERES	-	14
			DATA HORA				-	14 CARACTERES	-	28
			ID TERMINAL				-	8 CARACTERES	-	36
			STATUS DA TRANS			-	2 CARACTERES	-	38	
			NOME OU RAZAO SOCIAL	-	40 CARACTERES	-	78
			DATA NASC. OU ABERT.	-	8 CARACTERES	-	86
			ENDERECO DO TELEFONE	-	40 CARACTERES	-	126
			CIDADE DO TELEFONE		-	30 CARACTERES	-	156
			UF DO TELEFONE			-	2 CARACTERES	-	158
			TEXTO GEN�RICO			-	32 CARACTERES	-	190
			OUTROS TEXTOS			-	100 CARACTERES	-	290
			
	*/
	
	logapi.logtrc ("Inicia montagem da Resposta");
	
	psz_ToComp = new char[TAM_BUF_TRANS];
	memset(psz_ToComp,' ',TAM_BUF_TRANS);
	
	memcpy(psz_ToComp,"RX21",4);

	//Seta Resposta da Consulta
	trans = atoi(m_stRespostaConsulta.sz_NumConsulta);
	sprintf(transacao,"%010d",trans);
	memcpy(&psz_ToComp[POS_NSU],transacao,10);
	
	//Pega data e hora da resposta
	GetDataHoraSolicitacao();
	memcpy(&psz_ToComp[POS_DATA_HORA],datahora_solicitacao,14);
	
	// ID DO TERMINAL
	memcpy(&psz_ToComp[POS_ID_TERMINAL],m_pMySocket->m_stRespLogin.sz_IdTerm,8);
	
	// STATUS DA TRANSA��O
	memcpy(&psz_ToComp[POS_STATUS],&m_stRespostaConsulta.sz_CodResposta[0], 2);
		
	
	// PREENCHE A DADA PARA O CASO DE N�O VIR NADA
	memcpy(&psz_ToComp[POS_DATA],"00000000",8);

	if (m_stRespostaConsulta.ch_FlagOperAdic == 'S')
	{
		st_DETALHE_CONSULTA l_Dados;
		
		while (!m_queue_DETALHE.empty() ) {
			
			l_Dados = m_queue_DETALHE.front  ();
	
			if (!memcmp (m_stRespostaConsulta.sz_CodResposta, "89",2))
			{
				if (flg_dados == 0)
				{
					memcpy(&psz_ToComp[POS_OUTROS], l_Dados.sz_Titulo, 30);
					flg_dados = 1;
					logapi.logtrc ("l_Dados.sz_Titulo, [%s]", l_Dados.sz_Titulo);
				}
			}
			

			if ( !memcmp(l_Dados.sz_Titulo,"Titular",7) ) {
				
				memset(&psz_ToComp[POS_NOME], ' ', 40);
				if ( strlen(l_Dados.sz_Conteudo) < 40 )
					memcpy(&psz_ToComp[POS_NOME],l_Dados.sz_Conteudo,strlen(l_Dados.sz_Conteudo));
				else
					memcpy(&psz_ToComp[POS_NOME],l_Dados.sz_Conteudo,40);
				i_tc = i_tc + 40;

			}

			if ( !memcmp(l_Dados.sz_Titulo,"Razao Social",12) ) {				
				memset(&psz_ToComp[POS_NOME], ' ', 40);
				if ( strlen(l_Dados.sz_Conteudo) < 40) 
					memcpy(&psz_ToComp[POS_NOME],l_Dados.sz_Conteudo,strlen(l_Dados.sz_Conteudo));
				else
					memcpy(&psz_ToComp[POS_NOME],l_Dados.sz_Conteudo,40);				
				
				i_tc = i_tc + 40;				
			}
			
			
			if ( !memcmp(l_Dados.sz_Titulo,"Data de nascimento",18) ) {
				if ( !memcmp(l_Dados.sz_Conteudo, "00/00/0000", 10) ) {
					memcpy(s_data, "00000000", 8);				
				} else {					
					memcpy(s_data,&l_Dados.sz_Conteudo[0], 2);
					memcpy(&s_data[2],&l_Dados.sz_Conteudo[3], 2);
					memcpy(&s_data[4],&l_Dados.sz_Conteudo[6], 4);
				}
			    memcpy(&psz_ToComp[POS_DATA],s_data,8);
				i_tc = i_tc + 8;
			
			}
			
			
			if ( !memcmp(l_Dados.sz_Titulo,"Endereco do Telefone",20) ) {				
				memset(&psz_ToComp[POS_ENDERECO], ' ', 40);
				if ( strlen(l_Dados.sz_Conteudo) < 40 )
					memcpy(&psz_ToComp[POS_ENDERECO],l_Dados.sz_Conteudo,strlen(l_Dados.sz_Conteudo));
				else
					memcpy(&psz_ToComp[POS_ENDERECO],l_Dados.sz_Conteudo,40);
				i_tc = i_tc + 40 ;
				
			}
			
			
			if ( !memcmp(l_Dados.sz_Titulo,"Cidade do Telefone",18) ) {
			
				memset(&psz_ToComp[POS_CIDADE], ' ', 30);
				if ( strlen(l_Dados.sz_Conteudo) < 30 )
					memcpy(&psz_ToComp[POS_CIDADE],l_Dados.sz_Conteudo,strlen(l_Dados.sz_Conteudo));
				else
					memcpy(&psz_ToComp[POS_CIDADE],l_Dados.sz_Conteudo,30);
				i_tc = i_tc + 30 ;		
			
			}
			
			
			if ( !memcmp(l_Dados.sz_Titulo,"UF do Telefone",14) ) {
				
				memset(&psz_ToComp[POS_UF], ' ', 2);
				memcpy(&psz_ToComp[POS_UF],l_Dados.sz_Conteudo,2);
				i_tc = i_tc + 2 ;
			}
			
			m_queue_DETALHE.pop();
		} 
	} else {	
		if ( m_queue_DETALHE.empty() ) {
			// queue vazia !
			memset(&psz_ToComp[POS_NOME], ' ', 40);
			memcpy(&psz_ToComp[POS_DATA],"00000000",8);
			memset(&psz_ToComp[POS_ENDERECO], ' ', 40);
			memset(&psz_ToComp[POS_CIDADE], ' ', 30);
			memset(&psz_ToComp[POS_UF], ' ', 2);
		}
		
	}
	// TEXTO GEN�RICO = PRIMEIRO TEXTO + SEGUNDO TEXTO
	memcpy(&psz_ToComp[POS_TEXTO],&m_stRespostaConsulta.sz_PrimeiroTexto[0],16);
	memcpy(&psz_ToComp[POS_TEXTO + 16],&m_stRespostaConsulta.sz_SegundoTexto[0],16);
		

	//logapi.logdump ("psz_ToComp:", (char *)psz_ToComp, 288);
	
	memset(s_buffer_dll,' ', TAM_BUF_TRANS);
	memcpy(s_buffer_dll, &psz_ToComp[0],TAM_BUF_TRANS);
	i_len_bufdll = TAM_BUF_TRANS;
	
	((CThrTransacao *)p_ThrTrans)->PostThreadMessage(WM_USER + 5,0,0); // avisa que chegou uma resposta

	if ( psz_ToComp )
		delete [] psz_ToComp;

	LeaveCriticalSection(&m_CriticalSection);

	return 1;
}

void CSSLFrame::GetDataHoraSolicitacao(void)
{
	time_t ltime;	
	struct tm *today;
	char result[17];
	
	time( &ltime );

	today = localtime( &ltime );

	memset (result, '\0', sizeof(result));	
	strftime( result, 16, "%Y%m%d%H%M%S", today );    

	memset (datahora_solicitacao, '\0', sizeof(datahora_solicitacao));
	memcpy (datahora_solicitacao, result, 14);
	
	return;
}