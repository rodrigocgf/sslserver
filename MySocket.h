// MySocket.h: Schnittstelle f�r die Klasse CMySocket.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MYSOCKET_H__D972BDE0_B82B_4AAD_8232_2A5BAF040129__INCLUDED_)
#define AFX_MYSOCKET_H__D972BDE0_B82B_4AAD_8232_2A5BAF040129__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AsyncSocketEx.h"
#include "AsyncProxySocketLayer.h"
#include "AsyncSslSocketLayer.h"


#include <list>
#include <queue>
#include <map>

using namespace std;

class CMySocket : public CAsyncSocketEx
{
public:
	BOOL AddSslCertHashToTrustedList(unsigned char * pHash);
	virtual void OnConnect(int nErrorCode);
	virtual void OnReceive(int nErrorCode);
	CAsyncProxySocketLayer *m_pProxyLayer;
	CAsyncSslSocketLayer *m_pSslLayer;
	int OnLayerCallback(const CAsyncSocketExLayer *pLayer, int nType, int nParam1, int nParam2);

	//typedef std::list<unsigned char*> t_HashList;
	typedef list<unsigned char*> t_HashList;

	//CMySocket( CEdit *pResponse, t_HashList *pSslTrustedCertHashList);
	CMySocket( t_HashList *pSslTrustedCertHashList);
	virtual ~CMySocket();

	CFrameWnd *p_Parent;	
	BOOL b_LojaLogada;

	CRITICAL_SECTION m_CriticalSection;

	t_HashList::iterator iter;

	typedef struct {
		char sz_NomeTrans[4];
		char sz_TamStringResp[4];
			
	} st_TAM_RESP_LOGIN;
	st_TAM_RESP_LOGIN m_stTamRespLogin;


	typedef struct {
		char sz_IdMsg[3];
		char sz_IdTerm[8];
		char sz_CodResp[4];
		char sz_MsgResp[24];
		char sz_Timeout[2];
		char ch_flagConsCh;
		char ch_flagEstorno;
		char ch_CmdLogin;
		char sz_VerFirmTerm[7];
		char sz_filler[19];		
	} st_RESP_LOGIN;
	st_RESP_LOGIN m_stRespLogin;


	t_SslCertData *pData;
	MSG m_MSG;
	char s_recbuffer[4097];
	
protected:
	void AddStringToLog(LPCTSTR pszString);

	t_HashList *m_pSslTrustedCertHashList;
	
	//CEdit *m_pResponse;
};

#endif // !defined(AFX_MYSOCKET_H__D972BDE0_B82B_4AAD_8232_2A5BAF040129__INCLUDED_)
