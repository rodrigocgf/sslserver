// ThrTimer.cpp : implementation file
//

#include "stdafx.h"
#include "CHBSSLServer.h"
#include "ThrTimer.h"
#include "ThrListen.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CThrTimer

IMPLEMENT_DYNCREATE(CThrTimer, CWinThread)

CThrTimer::CThrTimer(CWinThread *p_Transacao)
{
	p_Thr = p_Transacao;
}

CThrTimer::CThrTimer()
{
}

CThrTimer::~CThrTimer()
{
}

BOOL CThrTimer::InitInstance()
{
	// TODO:  perform and per-thread initialization here
	Sleep(TIMEOUT*1000); // espera 90 segundos
	//Sleep(10000);

	((CThrListen *)p_Thr)->OnTimeout(this->m_nThreadID);
	//ExitThread(0);
	::PostThreadMessage(this->m_nThreadID,WM_QUIT,0,0);
	//PostThreadMessage(WM_QUIT,0,0);
	return TRUE;
}

int CThrTimer::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CThrTimer, CWinThread)
	//{{AFX_MSG_MAP(CThrTimer)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CThrTimer message handlers
