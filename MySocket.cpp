// MySocket.cpp: Implementierung der Klasse CMySocket.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "stdafx.h"
#include "MySocket.h"
#include "SSLFrame.h"
/*
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
*/

#include "LogApiFunc.h"
extern CLogApiFunc logapi;


//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

//CMySocket::CMySocket(CEdit *pResponse,  t_HashList *pSslTrustedCertHashList)
CMySocket::CMySocket(t_HashList *pSslTrustedCertHashList)
{
	
	ASSERT(pSslTrustedCertHashList);

	//m_pResponse = pResponse;
	m_pSslTrustedCertHashList = pSslTrustedCertHashList;

	//m_pProxyLayer = new CAsyncProxySocketLayer;
	m_pSslLayer = new CAsyncSslSocketLayer;
	
	b_LojaLogada = false;

	InitializeCriticalSection(&m_CriticalSection);
	
}

CMySocket::~CMySocket()
{
	Close();
	delete m_pSslLayer;
	//delete m_pProxyLayer;

	DeleteCriticalSection(&m_CriticalSection);
}

void CMySocket::OnReceive(int nErrorCode)
{
	int i_1;
	int i_tamdados;

	logapi.logtrc("CMySocket::OnReceive");
	EnterCriticalSection(&m_CriticalSection);

	if (nErrorCode)
	{
		//AfxMessageBox("Fatal error! Network subsystem failed!", MB_ICONSTOP);
		AddStringToLog("Fatal error! Network subsystem failed!");
		Close();
		return;
	}
	
	memset(s_recbuffer,0,sizeof(s_recbuffer));
	int res=Receive(&s_recbuffer, 4096);
	if (res==SOCKET_ERROR || !res)
	{
		if (GetLastError()!=WSAEWOULDBLOCK || !res)
		{
			AddStringToLog(_T("Error: Connection has been closed."));			
			return;
		}
		return;
	}
	s_recbuffer[res]=0;

	AddStringToLog(_T("Server reply:"));
	AddStringToLog(s_recbuffer);

	// Se a loja j� tiver sido logada
	if ( b_LojaLogada ) {
		if ( strlen(s_recbuffer) > 0 ) { // H� dados v�lidos de resposta
			p_Parent->PostMessage(WM_USER + 5,0,0); // Trata os dados recebidos

			LeaveCriticalSection(&m_CriticalSection);
			return;
		}
	}

	// Verifica se a loja foi logada com sucesso.
	for ( i_1 = 0 ; i_1 < (strlen(s_recbuffer) - 23) ; i_1++ ) {
		if ( !strncmp(&s_recbuffer[i_1],"LOJA LOGADA COM SUCESSO",23 ) ) {
			// Preenche a estrutura de resposta de login
			memcpy((char *)&m_stTamRespLogin,&s_recbuffer[0],sizeof(st_TAM_RESP_LOGIN) );
			i_tamdados = atoi(m_stTamRespLogin.sz_TamStringResp);
			i_tamdados -= 8;
			if (i_tamdados > 0 )
				memcpy((char *)&m_stRespLogin,&s_recbuffer[sizeof(st_TAM_RESP_LOGIN)], i_tamdados);			
			b_LojaLogada = true;
			p_Parent->PostMessage(WM_USER + 4,0,0); // Envia dados
			break;
		}
	}
	
	LeaveCriticalSection(&m_CriticalSection);
	return;	
}

void CMySocket::OnConnect(int nErrorCode)
{
	if (nErrorCode)
	{
		AddStringToLog("Error: Could not connect to server.");
		Close();
	} else {		
		AddStringToLog("Status: Connected to server.");		
	}
}

int CMySocket::OnLayerCallback(const CAsyncSocketExLayer *pLayer, int nType, int nParam1, int nParam2)
{
	
	char s_aux[100];
	//ASSERT(pLayer);	
	if (nType==LAYERCALLBACK_STATECHANGE)
	{	
		if (pLayer==m_pProxyLayer)
			sprintf(s_aux,"Layer Status: m_pProxyLayer changed state from %d to %d", nParam2, nParam1);			
		else if (pLayer==m_pSslLayer)
			sprintf(s_aux,"Layer Status: m_pSslLayer changed state from %d to %d", nParam2, nParam1);			
		else
			sprintf(s_aux,"Layer Status: Layer @ %d changed state from %d to %d", pLayer, nParam2, nParam1);
			
		AddStringToLog(s_aux);
		return 1;
	}
	else if (nType == LAYERCALLBACK_LAYERSPECIFIC)
	{
		if (pLayer == m_pProxyLayer)
		{
			switch (nParam1)
			{
			case PROXYERROR_NOCONN:
				AddStringToLog(_T("Proxy error: Can't connect to proxy server."));
				break;
			case PROXYERROR_REQUESTFAILED:
				AddStringToLog(_T("Proxy error: Proxy request failed, can't connect through proxy server."));
				if (nParam2)
					AddStringToLog((LPCTSTR)nParam2);
				break;
			case PROXYERROR_AUTHTYPEUNKNOWN:
				AddStringToLog(_T("Proxy error: Required authtype reported by proxy server is unknown or not supported."));
				break;
			case PROXYERROR_AUTHFAILED:
				AddStringToLog(_T("Proxy error: Authentication failed"));
				break;
			case PROXYERROR_AUTHNOLOGON:
				AddStringToLog(_T("Proxy error: Proxy requires authentication"));
				break;
			case PROXYERROR_CANTRESOLVEHOST:
				AddStringToLog(_T("Proxy error: Can't resolve host of proxy server."));
				break;
			default:
				AddStringToLog(_T("Proxy error: Unknown proxy error") );
			}
		}
		else if (pLayer == m_pSslLayer)
		{
			switch (nParam1)
			{
			case SSL_INFO:
				switch (nParam2)
				{
				case SSL_INFO_ESTABLISHED:
					AddStringToLog(_T("SSL connection established"));					
					p_Parent->PostMessage(WM_USER + 3,0,0);
					break;
				}
				break;
			case SSL_FAILURE:
				switch (nParam2)
				{
				case SSL_FAILURE_UNKNOWN:
					AddStringToLog(_T("Unknown error in SSL layer"));
					break;
				case SSL_FAILURE_ESTABLISH:
					AddStringToLog(_T("Could not establish SSL connection"));
					break;
				case SSL_FAILURE_LOADDLLS:
					AddStringToLog(_T("Failed to load OpenSSL DLLs"));
					break;
				case SSL_FAILURE_INITSSL:
					AddStringToLog(_T("Failed to initialize SSL"));
					break;
				case SSL_FAILURE_VERIFYCERT:
					AddStringToLog(_T("Could not verify SSL certificate"));
					break;
				}
				break;
			case SSL_VERBOSE_INFO:
			case SSL_VERBOSE_WARNING:
				AddStringToLog((char *)nParam2);
				break;
			case SSL_VERIFY_CERT:
				//t_SslCertData *pData = new t_SslCertData;				
				pData = new t_SslCertData;
				if (m_pSslLayer->GetPeerCertificateData(*pData))
				{					
				/* Do a first validation of the certificate here, return 1 if it's valid
				* and 0 if not.
				* If still unsure, let the user decide and return 2;
					*/
					iter = m_pSslTrustedCertHashList->begin();					
					
					for ( ; iter != m_pSslTrustedCertHashList->end(); iter++)					
						if (!memcmp(pData->hash, *iter, 20))
							return 1;
						
						//SetEvent(h_EsperaVerif);							
						
						((CSSLFrame *)p_Parent)->PostMessage(WM_USER + 2,0,0);
						//AfxGetMainWnd()->PostMessage(WM_USER, (WPARAM)pData, 0);					
						return 2;
				} else {
					//logapi.logtrc(" GetPeerCertificate retornou 0");
					return 1;

				}
			}
		}
	}

	return 1;
}

void CMySocket::AddStringToLog(LPCTSTR pszString)
{
	
	/*
	CString str;
	m_pResponse->GetWindowText(str);
	str+=pszString;
	str+=_T("\r\n");
	m_pResponse->SetWindowText(str);
	*/
	logapi.logtrc((char *)pszString);
	
}

BOOL CMySocket::AddSslCertHashToTrustedList(unsigned char * pHash)
{
	if (!pHash)
		return FALSE;
	for (t_HashList::iterator iter = m_pSslTrustedCertHashList->begin(); iter != m_pSslTrustedCertHashList->end(); iter++)
		if (!memcmp(pHash, *iter, 20))
			return FALSE;

	unsigned char* pData = new unsigned char[20];
	memcpy(pData, pHash, 20);
	m_pSslTrustedCertHashList->push_back(pData);
	
	return TRUE;
}
