// ThrTransacao.cpp : implementation file
//

#include "stdafx.h"
#include "CHBSSLServer.h"
#include "ThrTransacao.h"
#include "SSLFrame.h"

#include "LogApiFunc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CLogApiFunc logapi;

/////////////////////////////////////////////////////////////////////////////
// CThrTransacao

IMPLEMENT_DYNCREATE(CThrTransacao, CWinThread)

CThrTransacao::CThrTransacao()
{
}

CThrTransacao::CThrTransacao(WPARAM wParam)
{
	sock_accepted = wParam;
	h_FimThr = CreateEvent(NULL,FALSE,FALSE,NULL);	
	h_CanKillThr = CreateEvent(NULL,FALSE,FALSE,NULL);	
	i_stxetx = WAIT_STX;
	i_indexmsg = 0;
	memset(s_buffer_trans,0,sizeof(s_buffer_trans));

	p_SSLWindow = NULL;
	//p_SSLWindow = new CSSLFrame;
	/*
	((CSSLFrame *)p_SSLWindow)->p_ThrTrans = this;
	p_SSLWindow->LoadFrame(IDR_MAINFRAME,WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, NULL,	NULL);
	//p_SSLWindow->ShowWindow(SW_SHOW);
	p_SSLWindow->ShowWindow(SW_HIDE);
	p_SSLWindow->UpdateWindow();
	*/
}

CThrTransacao::~CThrTransacao()
{
	
	if ( p_SSLWindow != NULL ) {
		//((CSSLFrame *)p_SSLWindow)->m_pMySocket->Close();
		
		// FECHA OS SOCKETS ANTES DE SAIR
		if ( sock_accepted != INVALID_SOCKET )
			closesocket(sock_accepted);		
		
		delete p_SSLWindow;
	}

	logapi.logtrc(LOG_LEVEL_DEFAULT,"<--- FIM DA TRANSA��O AP�S 90 segundos --->");
}

BOOL CThrTransacao::InitInstance()
{
	// TODO:  perform and per-thread initialization here
	logapi.logtrc(LOG_LEVEL_DEFAULT,"<--- IN�CIO DA TRANSA��O --->");
	TrataTransacoes();
	return TRUE;
}

int CThrTransacao::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CThrTransacao, CWinThread)
	//{{AFX_MSG_MAP(CThrTransacao)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
	ON_THREAD_MESSAGE(WM_USER+5,OnDadosRecebidos)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CThrTransacao message handlers

void CThrTransacao::TrataTransacoes()
{
	DWORD dw_Bytes_In_Buffer;
	DWORD dw_Bytes_Received;
	BOOL b_flg_loop = true;
	BOOL b_msg_OK = false;
	int i_1;
	int i_ret;
	//char *p_send;

	//while ( ( WaitForSingleObject(h_FimThr,150) != WAIT_OBJECT_0 ) ) {		
	while ( (b_msg_OK == false) && ( WaitForSingleObject(h_FimThr,0) != WAIT_OBJECT_0 ) ) {


		dw_Bytes_In_Buffer = 0;
		if ( ioctlsocket(sock_accepted,FIONREAD,&dw_Bytes_In_Buffer)) {
			logapi.logtrc(LOG_LEVEL_DEFAULT,"Erro na fun��o ioctlsocket %d",WSAGetLastError());						
			//Sleep(100);
			SetEvent(h_FimThr);
			continue;
		}
				
		if ( (dw_Bytes_In_Buffer > 0) && (dw_Bytes_In_Buffer < MAX_DADOS)  ) {
			p_recv_buf = new BYTE[dw_Bytes_In_Buffer];
			dw_Bytes_Received = recv(sock_accepted,(char *)&p_recv_buf[0],dw_Bytes_In_Buffer,0);

			if ( (dw_Bytes_Received == SOCKET_ERROR) && ( dw_Bytes_Received != WSAEWOULDBLOCK )  ) {			
				logapi.logtrc(LOG_LEVEL_DEFAULT,"TrataTransacoes  >>  Erro em recv");
			} else {
				if ( dw_Bytes_Received > 0 ) {
					logapi.logdump("BYTES RECEBIDOS ",(char *)p_recv_buf,dw_Bytes_Received);			
					for ( i_1 = 0 ; ( (i_1 < dw_Bytes_In_Buffer) && b_flg_loop ) ; i_1++ ) {
						switch (i_stxetx) {
						case WAIT_STX:
							if ( p_recv_buf[i_1] == STX ) {
								i_stxetx = WAIT_ETX;
								i_indexmsg = 0;
								s_buffer_trans[i_indexmsg] = p_recv_buf[i_1];
								i_indexmsg++;
							}
							break;
						case WAIT_ETX:
							s_buffer_trans[i_indexmsg] = p_recv_buf[i_1];
							i_indexmsg++;	
							if ( p_recv_buf[i_1] == ETX ) {
								i_stxetx = WAIT_STX;	
								b_flg_loop = false;
								b_msg_OK = true;
								
								logapi.logdump("Mensagem em s_buffer_trans : ",s_buffer_trans ,i_indexmsg);								
							} 

							if ( i_indexmsg > MAX_DADOS ) {
								b_flg_loop = false;
								i_indexmsg = 0;
								b_msg_OK = false;
							} 

							break;
						}
					}
				}
			}
			delete [] p_recv_buf;	
		}
	}

	if ( (i_indexmsg > 0) && b_msg_OK ) { // H� uma mensagem v�lida em s_buffer_trans
		// A mensagem a ser recebida da ConsultaCheckForte.dll � do tipo
		// STX <buffer_in> ;  <ip_endereco> ; <ip_porta> ; ETX
		char seps[] = ";";
		char *token;		

		token = strtok(s_buffer_trans,seps);
		i_1 = 0;
		while ( token != NULL ) {
			switch (i_1) {
			case 0:
				strcpy(m_s_buffer_in,&token[1]);
				break;					
			case 1:
				strcpy(m_s_ip_endereco,token);
				break;
			case 2:
				strcpy(m_s_ip_porta,token);
				break;
			}
			i_1++;
			token = strtok(NULL,seps);
		}

		
		p_SSLWindow = new CSSLFrame;		
		
		((CSSLFrame *)p_SSLWindow)->p_ThrTrans = this;
		p_SSLWindow->LoadFrame(IDR_MAINFRAME,WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, NULL,	NULL);
		//p_SSLWindow->ShowWindow(SW_SHOW);
		p_SSLWindow->ShowWindow(SW_HIDE);
		p_SSLWindow->UpdateWindow();
		
			
		i_ret = ((CSSLFrame *)p_SSLWindow)->ConsultaCheque(m_s_buffer_in,m_s_buffer_out,m_s_ip_endereco,m_s_ip_porta);
		
	}

	if ( WaitForSingleObject(h_FimThr,0) == WAIT_OBJECT_0 ) {
		// FECHA OS SOCKETS ANTES DE SAIR
		if ( sock_accepted != INVALID_SOCKET )
			closesocket(sock_accepted);

		if ( p_SSLWindow != NULL ) {
			delete p_SSLWindow;
			p_SSLWindow = NULL;
		}		
		//::PostThreadMessage(this->m_nThreadID,WM_QUIT,0,0);
	}

	SetEvent(h_CanKillThr);

	return;
}

LRESULT CThrTransacao::OnDadosRecebidos(WPARAM wParam, LPARAM lParam)
{

	// Envia um buffer de volta para ConsultaCheckForte.dll
	// STX i_ret ; buffer_out ; ETX 
	// buffer_out tem tamanho m�ximo de 300 bytes
	fd_set FdWrite;

	FD_SET(sock_accepted,&FdWrite);
	//int nRet;
	struct timeval TimeOut;
	TimeOut.tv_sec  = 30;
	TimeOut.tv_usec = 0;
	int i_ret = TAM_BUF_TRANS;

	//if ( ( nRet = select( 1,NULL,&FdWrite,NULL,&TimeOut) ) == SOCKET_ERROR ) {
	//	logapi.logtrc(" Erro ao esperar permiss�o para escrita no socket com a dll");
	//} else {
		p_send = new char[305];
		
		p_send[0] = STX;
		sprintf(&p_send[1],"%03d",i_ret);
		p_send[4] = ';';
		//memcpy(&p_send[5],((CSSLFrame *)p_SSLWindow)->s_buffer_dll,TAM_BUF_TRANS);
		memcpy(&p_send[5],((CSSLFrame *)p_SSLWindow)->s_buffer_dll,((CSSLFrame *)p_SSLWindow)->i_len_bufdll);		

		//p_send[5+TAM_BUF_TRANS] = ';';
		p_send[5+((CSSLFrame *)p_SSLWindow)->i_len_bufdll] = ';';
		//p_send[5+TAM_BUF_TRANS+1] = ETX;
		p_send[5+((CSSLFrame *)p_SSLWindow)->i_len_bufdll+1] = ETX;

		//send(sock_accepted ,&p_send[0],(7 + TAM_BUF_TRANS),0);
		send(sock_accepted ,&p_send[0],(7 + ((CSSLFrame *)p_SSLWindow)->i_len_bufdll),0);

		logapi.logdump("MENSAGEM ENVIADA PARA A DLL : ",p_send,(6+((CSSLFrame *)p_SSLWindow)->i_len_bufdll));
		delete [] p_send;
	//}

	//PostThreadMessage(WM_QUIT,0,0);

	return 1;
}