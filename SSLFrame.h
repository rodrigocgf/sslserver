#if !defined(AFX_SSLFRAME_H__DFE75AF2_2E7E_4057_B394_3E63975B76B1__INCLUDED_)
#define AFX_SSLFRAME_H__DFE75AF2_2E7E_4057_B394_3E63975B76B1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SSLFrame.h : header file
//

/* 
	FORMATO DO PACOTE RECEBIDO DO CONSULTCHEQUE:

    RX21						- 4 CARACTERES		4
	<IDENTIFICAÇÃO DO TERMINAL> - 8 CARACTERES		12
	<CNPJ_LOJA>					- 15 CARACTERES		27
	<NUMERO DO CONTRATO>		- 5 CARACTERES		32
	<SENHA DO CONTRATO>			- 5 CARACTERES		37	
	<TIPO DO DOCUMENTO:CGC/CPF>	- 1 CARACTER		38
	<CNPJ_CPF>					- 15 CARACTERES		53
	<DDD>						- 3 CARACTERES		56
	<TELEFONE>					- 8 CARACTERES		64
	<FORMA DE ENTRADA DE DADOS> - 1 CARACTER		65
	<QUANTIDADE DE CHEQUES>		- 2 CARACTERES		67
	<CMC7>						- 30 CARACTERES		97
	<DATA DO CHEQUE>			- 8 CARACTERES		105
	<VALOR DO CHEQUE EM CENT.>	- 15 CARACTERES		120

*/

/*
		
	  LAYOUT DO BUFFER PARA O COMPONENTE
	  
		RX21					-	4 CARACTERES	-	4	
		NSU						-	10 CARACTERES	-	14
		DATA HORA				-	14 CARACTERES	-	28
		ID TERMINAL				-	8 CARACTERES	-	36
		STATUS DA TRANS			-	2 CARACTERES	-	38	
		NOME OU RAZAO SOCIAL	-	40 CARACTERES	-	78
		DATA NASC. OU ABERT.	-	8 CARACTERES	-	86
		ENDERECO DO TELEFONE	-	40 CARACTERES	-	126
		CIDADE DO TELEFONE		-	30 CARACTERES	-	156
		UF DO TELEFONE			-	2 CARACTERES	-	158
		TEXTO GENÉRICO			-	32 CARACTERES	-	190
		OUTROS TEXTOS			-	100 CARACTERES	-	290
		
*/
#define POS_RX21			0
#define POS_NSU				4
#define POS_DATA_HORA		14
#define POS_ID_TERMINAL		28
#define POS_STATUS			36
#define POS_NOME			38
#define POS_DATA			78
#define POS_ENDERECO		86
#define POS_CIDADE			126
#define POS_UF				156
#define POS_TEXTO			158
#define POS_OUTROS			190

#define TAM_BUF_TRANS	288

#include "stdafx.h"
#include "ChildView.h"
#include "MySocket.h"

#include <list>
#include <iostream>
#include <queue>
#include <map>

using namespace std;


#define TAM_BUF_RECEB_NEW		490
#define	TAM_BUF_DLL				350

/////////////////////////////////////////////////////////////////////////////
// CSSLFrame frame

class CSSLFrame : public CFrameWnd
{
	DECLARE_DYNCREATE(CSSLFrame)
public:
	CSSLFrame();           // protected constructor used by dynamic creation
	virtual ~CSSLFrame();

// Attributes
public:
	CChildView    m_wndView;
	HANDLE h_RespOK;
	CMySocket *m_pMySocket;
	CWinThread *p_ThrTrans;

	//typedef std::list<unsigned char *> t_HashList;
	typedef list<unsigned char *> t_HashList;

	t_HashList m_SslTrustedCertHashList;

	char * m_buffer_in;
	char * m_buffer_out; 
	char * m_ip_endereco; 
	char * m_ip_porta;

	char	buffer_rec[TAM_BUF_RECEB_NEW];
	char	s_buffer_dll[TAM_BUF_DLL];
	int		i_len_bufdll;

	CRITICAL_SECTION m_CriticalSection;

	// VARIÁVEIS
	typedef struct {
		char sz_NomeTrans[4];
		char sz_TamString[4];
		char sz_IDMsg[3];
		char sz_IdTerm[8];
		char sz_SenhaLoja[8];
		char sz_Delay[4];
		char sz_TamBlocosResp[4];
		char ch_flagBanner;
		char sz_filler[22];
		char ch_StatusMem;
		char sz_Terminador[2];
		char sz_Enter[1];
	} st_LOGIN;
	st_LOGIN m_stLogin;

	typedef struct {
		char sz_NomeTrans[4];
		char sz_TamStringResp[4];
			
	} st_TAM_RESP_LOGIN;
	st_TAM_RESP_LOGIN m_stTamRespLogin;


	typedef struct {
		char sz_IdMsg[3];
		char sz_IdTerm[8];
		char sz_CodResp[4];
		char sz_MsgResp[24];
		char sz_Timeout[2];
		char ch_flagConsCh;
		char ch_flagEstorno;
		char ch_CmdLogin;
		char sz_VerFirmTerm[7];
		char sz_filler[19];		
	} st_RESP_LOGIN;
	st_RESP_LOGIN m_stRespLogin;
	

	char sz_TxtBanner[96];


	typedef struct { 
		char sz_NomeTrans[4];
		char ch_VersaoLayout;
		char sz_IdTipoMsg[3];
		char ch_flag_Continua;
		char sz_CNPJ_LOJA[15];
		char ch_TipoTerminal;
		char ch_TipoConexao;
		char sz_NumeroContrato[5];
		char sz_SenhaContrato[5];
		char ch_MotivoTransacao;
		char ch_TipoDocumento;
		char sz_CNPJ_CPF[15];
		char sz_DDD[3];
		char sz_Telefone[8];
		char sz_OperacoesAdicionais[5];
		char sz_QtdeCheques[4];
		//
		char ch_FormaEntrada;
		char sz_CMC7[30];
		char sz_DataCheque[8];
		char sz_ValorChequeCentavos[15];
	} st_SOLICITACAO_CONSULTA;
	st_SOLICITACAO_CONSULTA m_stSolicitacaoConsulta;


	typedef struct {
		char ch_FlagInfDestino;
		char sz_Titulo[30];
		char sz_Conteudo[100];
	} st_DETALHE_CONSULTA;
	st_DETALHE_CONSULTA m_DETALHE_CONSULTA;
	
	
	queue<st_DETALHE_CONSULTA> m_queue_DETALHE;

	typedef struct {
		char sz_NomeTrans[4];
		char ch_VersaoLayout;
		char sz_IdTipoMsg[3];
		char sz_QtdeCampos[3];
		char ch_FlagOperAdic;
		char ch_FlagHaMaisCampos;
		char sz_CodResposta[2];
		char sz_PrimeiroTexto[16];
		char sz_SegundoTexto[16];
		char ch_FlagChequesConsultados;
		char sz_NumConsulta[20];
		char sz_DataConsulta[10];
		char sz_HoraConsulta[8];
		//
		char sz_OperAdicional[10];
		char ch_FlagTemMaisOperAdic;
		char sz_QtdeInformacoes[5];
	} st_RESPOSTA_CONSULTA;
	st_RESPOSTA_CONSULTA m_stRespostaConsulta;

	map<int,int> m_MapBarras;
	char datahora_solicitacao[14];
	char sz_Qtd_Campos[6];
	char cod_status[1];


// Operations
public:
	int ConsultaCheque (char *, char *, char *, char *);
	int IniciaComunicacao(char * ip_endereco, char * ip_porta);
	void AddStringToLog(LPCTSTR pszString);
	void GetDataHoraSolicitacao(void);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSSLFrame)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:
	

	// Generated message map functions
	//{{AFX_MSG(CSSLFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	afx_msg LRESULT OnVerifyCert(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSSLConnect(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnLojaLogada(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnDados(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SSLFRAME_H__DFE75AF2_2E7E_4057_B394_3E63975B76B1__INCLUDED_)
