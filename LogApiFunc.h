// LogApiFunc.h: interface for the CLogApiFunc class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOGAPIFUNC_H__DCD40081_F9F1_11D3_93A8_00A0C93D342D__INCLUDED_)
#define AFX_LOGAPIFUNC_H__DCD40081_F9F1_11D3_93A8_00A0C93D342D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CLogApiFunc  
{
public:
	void logtrc ( char * msg, ... );
	long getlevel();
	long loglevel;
	void gravalog ( char  * msg, char tipo );
	void logerr ( char * msg, ... );
	void logtrc ( long log_level, char * msg,...);
	void logsys ( char * msg, ...);

	void hexdump (FILE *f, char * buff, int len);
	void logdump( char * title, char * buf, int siz);
	void printlin (FILE *f, char * buff, int offs, int len, int i_tambuff);

   CLogApiFunc();
	virtual ~CLogApiFunc();
	char m_dir [1024];
	char m_file [1024];
};

#endif // !defined(AFX_LOGAPIFUNC_H__DCD40081_F9F1_11D3_93A8_00A0C93D342D__INCLUDED_)
