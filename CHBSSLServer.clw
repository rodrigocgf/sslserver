; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CThrTransacao
LastTemplate=CFrameWnd
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "CHBSSLServer.h"
LastPage=0

ClassCount=8
Class1=CCHBSSLServerApp
Class3=CMainFrame
Class4=CAboutDlg

ResourceCount=2
Resource1=IDR_MAINFRAME
Class2=CChildView
Class5=CThrListen
Class6=CThrTimer
Class7=CThrTransacao
Class8=CSSLFrame
Resource2=IDD_ABOUTBOX

[CLS:CCHBSSLServerApp]
Type=0
HeaderFile=CHBSSLServer.h
ImplementationFile=CHBSSLServer.cpp
Filter=N

[CLS:CChildView]
Type=0
HeaderFile=ChildView.h
ImplementationFile=ChildView.cpp
Filter=N

[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
LastObject=CMainFrame




[CLS:CAboutDlg]
Type=0
HeaderFile=CHBSSLServer.cpp
ImplementationFile=CHBSSLServer.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_APP_EXIT
Command2=ID_EDIT_UNDO
Command3=ID_EDIT_CUT
Command4=ID_EDIT_COPY
Command5=ID_EDIT_PASTE
Command6=ID_APP_ABOUT
CommandCount=6

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_EDIT_COPY
Command2=ID_EDIT_PASTE
Command3=ID_EDIT_UNDO
Command4=ID_EDIT_CUT
Command5=ID_NEXT_PANE
Command6=ID_PREV_PANE
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_CUT
Command10=ID_EDIT_UNDO
CommandCount=10

[CLS:CThrListen]
Type=0
HeaderFile=ThrListen.h
ImplementationFile=ThrListen.cpp
BaseClass=CWinThread
Filter=N

[CLS:CThrTimer]
Type=0
HeaderFile=ThrTimer.h
ImplementationFile=ThrTimer.cpp
BaseClass=CWinThread
Filter=N

[CLS:CThrTransacao]
Type=0
HeaderFile=ThrTransacao.h
ImplementationFile=ThrTransacao.cpp
BaseClass=CWinThread
Filter=N
LastObject=CThrTransacao

[CLS:CSSLFrame]
Type=0
HeaderFile=SSLFrame.h
ImplementationFile=SSLFrame.cpp
BaseClass=CFrameWnd
Filter=T
LastObject=CSSLFrame
VirtualFilter=fWC

