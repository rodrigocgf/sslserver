#if !defined(AFX_THRLISTEN_H__99B5447F_8B6D_431D_A3E2_A4144E622826__INCLUDED_)
#define AFX_THRLISTEN_H__99B5447F_8B6D_431D_A3E2_A4144E622826__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ThrListen.h : header file
//

#include "ThrTransacao.h"
#include "ThrTimer.h"

#define PORT_LISTEN	7300

/////////////////////////////////////////////////////////////////////////////
// CThrListen thread

class CThrListen : public CWinThread
{
	DECLARE_DYNCREATE(CThrListen)
public:
	CThrListen();           // protected constructor used by dynamic creation
	virtual ~CThrListen();

// Attributes
public:
	SOCKET sock_LISTEN;
	SOCKADDR_IN addr_LISTEN;
	CString status_sock_LISTEN;

	SOCKET sock_ACCEPTED;
	SOCKADDR_IN addr;
	CString status_sock_ACCEPTED;

	CThrTransacao *p_Trans;
	CThrTimer *p_Timer;
	//   CTransacao -----> CTimer
	CMap<unsigned long,unsigned long &,unsigned long ,unsigned long &> m_MapConexoes;
	CTypedPtrArray<CPtrArray,CWinThread *> m_ThrList;

	//   CTransacao -----> sock_ACCEPTED_ROUTER
	CMap<unsigned long,unsigned long &,WPARAM, WPARAM &> m_MapSOCKET;

	CRITICAL_SECTION m_CriticalSection;

// Operations
public:
	void InicializaPortListen();
	void OnTimeout(unsigned long ul_Timer);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CThrListen)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
protected:
	//virtual ~CThrListen();

	// Generated message map functions
	//{{AFX_MSG(CThrListen)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_THRLISTEN_H__99B5447F_8B6D_431D_A3E2_A4144E622826__INCLUDED_)
