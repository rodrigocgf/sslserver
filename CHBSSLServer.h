// CHBSSLServer.h : main header file for the CHBSSLSERVER application
//

#if !defined(AFX_CHBSSLSERVER_H__91B84BE9_8C47_47D1_A081_F3B36D60EB93__INCLUDED_)
#define AFX_CHBSSLSERVER_H__91B84BE9_8C47_47D1_A081_F3B36D60EB93__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "MainFrm.h"

/////////////////////////////////////////////////////////////////////////////
// CCHBSSLServerApp:
// See CHBSSLServer.cpp for the implementation of this class
//

class CCHBSSLServerApp : public CWinApp
{
public:
	CCHBSSLServerApp();

	BOOL IniciaAplicacao();
	BOOL FinalizaAplicacao();
	LPCTSTR FindOneOf(LPCTSTR p1, LPCTSTR p2);

	CMainFrame *pFrame;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCHBSSLServerApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

public:
	//{{AFX_MSG(CCHBSSLServerApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHBSSLSERVER_H__91B84BE9_8C47_47D1_A081_F3B36D60EB93__INCLUDED_)
