#if !defined(AFX_THRTRANSACAO_H__211D87B8_05E2_4667_A229_33EA3FE91F36__INCLUDED_)
#define AFX_THRTRANSACAO_H__211D87B8_05E2_4667_A229_33EA3FE91F36__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ThrTransacao.h : header file
//
//#include "ComunicacaoCheckForte.h"

#define	WAIT_STX	0
#define WAIT_ETX	1
#define	MAX_DADOS	800
#define	STX			0x02
#define	ETX			0x03

#define TAM_BUF_TRANS	288

/////////////////////////////////////////////////////////////////////////////
// CThrTransacao thread

class CThrTransacao : public CWinThread
{
	DECLARE_DYNCREATE(CThrTransacao)
public:
	CThrTransacao();           // protected constructor used by dynamic creation
	CThrTransacao(WPARAM wParam);
	virtual ~CThrTransacao();

// Attributes
public:
	HANDLE h_FimThr;
	HANDLE h_CanKillThr;
	SOCKET sock_accepted;
	unsigned char *p_recv_buf;
	char  s_buffer_trans[MAX_DADOS];

	char  m_s_buffer_in[300];
	char  m_s_buffer_out[300];
	char  m_s_ip_endereco[40];
	char  m_s_ip_porta[20];

	int i_stxetx;
	int i_indexmsg;

	//CComunicacaoCheckForte m_ComunicacaoCheckForte;
	CFrameWnd *p_SSLWindow;
	char *p_send;
// Operations
public:
	void TrataTransacoes();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CThrTransacao)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
protected:	

	// Generated message map functions
	//{{AFX_MSG(CThrTransacao)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	afx_msg LRESULT OnDadosRecebidos(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_THRTRANSACAO_H__211D87B8_05E2_4667_A229_33EA3FE91F36__INCLUDED_)
