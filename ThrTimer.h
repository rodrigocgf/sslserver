#if !defined(AFX_THRTIMER_H__19CF6BE3_6068_4FBC_B5C6_00233FCE010D__INCLUDED_)
#define AFX_THRTIMER_H__19CF6BE3_6068_4FBC_B5C6_00233FCE010D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ThrTimer.h : header file
//

// 1 MINUTOS
#define TIMEOUT	60

/////////////////////////////////////////////////////////////////////////////
// CThrTimer thread

class CThrTimer : public CWinThread
{
	DECLARE_DYNCREATE(CThrTimer)
public:
	CThrTimer(CWinThread *p_Transacao);
	CThrTimer();           // protected constructor used by dynamic creation
	virtual ~CThrTimer();
// Attributes
public:
	CWinThread* p_Thr;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CThrTimer)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
protected:	

	// Generated message map functions
	//{{AFX_MSG(CThrTimer)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_THRTIMER_H__19CF6BE3_6068_4FBC_B5C6_00233FCE010D__INCLUDED_)
